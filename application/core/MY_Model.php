<?php if (!defined('BASEPATH'))exit('No direct script access allowed');
class MY_model extends CI_Model
{
	function __construct()
	{
		$this->load->library('user_agent');
	}
	public function insertar($collection, $data)
	{
		$id = $this->cimongo->insert($collection, $data);
		if ( isset($id) )
		{
			$this->set_log('inserta', $collection, $data['_id'], implode(',', array_keys($data)));			
			return $id;
		}else{
			$this->set_error_log('inserta', $collection, "Error al tratar de insertar");
			return FALSE;
		}
	}	

	public function seleccionar($collection, $campos=array(), $where = array(), $limit='')
	{

		if( !empty($campos) )
		{
			if( !is_array($campos) )
			{
				$campos = array((string)$campos);
			}
			$this->cimongo->select($campos);
		}

		if( !empty($where) && is_array($where) )
		{
			$this->cimongo->where($where);
		}
		$result = $this->cimongo->get($collection);
		if ( $result->num_rows() === 0 )
		{
			return FALSE;
		}else{
			if( empty($campos) )
			{
				$campos = $result->row_array();
			}
			if ( !empty($limit) && is_numeric($limit) && $limit == 1)
			{
				$response = (object)$result->row();
				$this->set_log('consulta', $collection, $response->_id, implode(',', array_keys($campos)));
				return $response;
			}else{
				$condicion = array();
				if( !empty($where) )
				{
					foreach ($where as $field => $value) {
						$condicion[] = "$field => $value";
					}
				}else{
					$condicion[] = 'ALL';
				}
				$this->set_log('consulta', $collection, implode(',', $condicion), implode(',', array_keys($campos)));
				return $result->result();
			}
		}
	}

	public function borrar($collection, $_id)
	{
		$rows = $this->cimongo
				->where(array('_id' => new MongoID($_id)))
				->get($collection)
				->row_array();
		$this->cimongo->insert($collection."_delete", $rows);

		$delete = $this->cimongo
					->where(array('_id' => new MongoID($_id)))
					->delete($collection);
		if( $delete )
		{
			$this->set_log('Elimina', $collection, $_id, implode(',', array_keys($rows)));
			return TRUE;
		}else{
			$this->set_error_log('Elimina', $collection, "Error al tratar de eliminar");
			return FALSE;
		}

	}

	public function actualizar($collection, $data, $_id)
	{
		$row = $this->cimongo
				->where(array('_id' => new MongoID($_id)))
				->get($collection)
				->row_array();
		$updated = $this->cimongo
					->where(array('_id' => new MongoID($_id)))
					->set($data)
					->update($collection);
		if ( $updated )
		{
			$campos = array();
			foreach ($data as $field => $value) {
				@ $campos[] = $field."_new =>". is_array($value)?implode(',', $value):$value.":".$field."_old => ".@$row[$field];
			}
			$this->set_log('Actualiza', $collection, $_id, implode(',', array_keys($campos)));
			return TRUE;
		}else{
			$this->set_error_log('Actualiza', $collection, "Error al tratar de actualizar");
			return FALSE;
		}
	}
	/**
		* @var $user
		* @var date('Y-m-d')
		* @param $operacion (inserta/seleccionar/borrar/actualizar)
		* @var $collection 
		* @var MongoID
		* @var $user_agent
		* @var $ip_address
	**/
	protected function set_log($operacion, $collection, $_id, $campos)
	{
		$user = $this->ion_auth->user()->row();
		$data = array(
				'collection' => (string)$collection,
				'fecha'		 => date('Y-m-d H:i:s'),
				'ip_address' => $this->input->ip_address(),
				'object'	 => $_id,
				'user_id'	 => $user->id,
				'movimiento' => (string)$operacion,
				'user_agent' => $this->agent->agent_string(),
				'campos'	 => (string)$campos
			);
		$id = $this->cimongo->insert('log', $data);
		return $this;
	}

	protected function set_error_log($operacion, $collection, $message)
	{
		$user = $this->ion_auth->user()->row();
		$data = array(
				'collection' => (string)$collection,
				'fecha'		 => date('Y-m-d H:i:s'),
				'ip_address' => $this->input->ip_address(),
				'message'	 => (string)$message,
				'user_id'	 => $user->id,
				'movimiento' => (string)$operacion,
				'user_agent' => $this->agent->agent_string()
			);
		$this->cimongo->insert('error_log', $data);
		return $this;
	}
}	