<?php 
if(!isset($title)){
    $title = "Sin Titulo";
}
$this->load->view('sb-admin/header', array('css'=>$css, 'title'=>$title));
?>

<?php $this->load->view('sb-admin/navbar')?>



        <!-- Page Content -->
        <div id="page-wrapper">
            <?php echo $content;?>
        </div>
        <!-- /#page-wrapper -->

    
<?php $this->load->view('sb-admin/footer', $js)?>
