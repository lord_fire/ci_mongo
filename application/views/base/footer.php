</div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?php echo base_url('site_media/lib/jquery/dist/jquery.min.js');?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('site_media/lib/bootstrap/dist/js/bootstrap.min.js');?>"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url('site_media/lib/metisMenu/dist/metisMenu.min.js');?>"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url('site_media/sb-admin/js/sb-admin-2.js');?>"></script>

</body>

</html>