<?php 
if(!isset($title)){
    $title = "Sin Titulo";
}
$this->load->view('base/header', array('css'=>$css, 'title'=>$title));
?>

<?php $this->load->view('base/navbar')?>


<?php echo $content;?>

    
<?php $this->load->view('base/footer', $js)?>
